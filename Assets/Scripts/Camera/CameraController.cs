﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    private const string MOUSE_WHEEL = "Mouse ScrollWheel";
    private const string MOUSE_X_AXIS = "Mouse X";
    private const string MOUSE_Y_AXIS = "Mouse Y";
    private const int MIDDLE_MOUSE_BTN = 2;
    private const int LEFT_MOUSE_BTN = 0;
    private const float CIRCLE_ANGLE = 360f;
    private const KeyCode ZOOM_IN_KEY = KeyCode.Z;
    private const KeyCode ZOOM_OUT_KEY = KeyCode.X;
    private const KeyCode RESET_CAM_KEY = KeyCode.C;
    private const KeyCode LEFT_SHIFT = KeyCode.LeftShift;
    private const KeyCode RIGHT_SHIFT = KeyCode.RightShift;

    [SerializeField]
    private Transform target;
    [SerializeField]
    private float maxDistance = 20;
    [SerializeField]
    private float minDistance = 0.6f;
    [SerializeField]
    private float degSpeed = 200.0f;
    [SerializeField]
    private float yMax = 85f;
    [SerializeField]
    private float zoomSpeed = 40;
    [SerializeField]
    private float dragSpeed = 3f;
    [SerializeField]
    private float interpolationSpeed = 500f;
    [SerializeField]
    private float inputOrbitCoeff = 0.02f;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float zoomKeyCoeff = 0.25f;

    private float distance;
    private float xDeg;
    private float yDeg;
    private float currentDistance;
    private float targetDistance;
    private Vector3 targetPosition;
    private Vector3 initialPos;
    private Quaternion initialRot;

    private void OnEnable()
    {
        Reset();
        initialPos = transform.position;
        initialRot = transform.rotation;
    }

    private void Reset()
    {
        targetPosition = target.position;

        distance = Vector3.Distance(transform.position, targetPosition);
        distance = Mathf.Clamp(distance, minDistance, maxDistance);

        currentDistance = distance;
        targetDistance = distance;

        ApplyPosition();

        xDeg = Vector3.Angle(Vector3.right, transform.right);
        yDeg = Vector3.Angle(Vector3.up, transform.up);
    }


    private void LateUpdate()
    {
        float dt = Time.deltaTime;
        bool resetCam = Input.GetKeyDown(RESET_CAM_KEY);
        bool leftMouseBtnPressed = Input.GetMouseButton(LEFT_MOUSE_BTN);
        bool shiftPressed = Input.GetKey(LEFT_SHIFT) || Input.GetKey(RIGHT_SHIFT);

        if (resetCam)
        {
            transform.position = initialPos;
            transform.rotation = initialRot;
            Reset();
            return;
        }

        if (leftMouseBtnPressed)
        {
            if(!shiftPressed)
            {
                ApplyOrbitRotation(dt);
            }
            else
            {
                ApplyDrag(leftMouseBtnPressed, shiftPressed);
            }
        }
        else
        {
            ApplyDrag(leftMouseBtnPressed, shiftPressed);
        }

        ApplyZoom(dt);
        ApplyPosition();
    }

    private void ApplyDrag(bool leftMouseBtnPressed, bool shiftPressed)
    {
        bool middleMouseBtnPressed = Input.GetMouseButton(MIDDLE_MOUSE_BTN);

        if(middleMouseBtnPressed)
        {
            DragCamera();
            return;
        }

        if(shiftPressed)
        {
            if(leftMouseBtnPressed)
            {
                DragCamera();
            }
        }
    }

    private void DragCamera()
    {
        targetPosition -= transform.right * Input.GetAxis(MOUSE_X_AXIS) * dragSpeed;
        targetPosition -= transform.up * Input.GetAxis(MOUSE_Y_AXIS) * dragSpeed;
    }

    private void ApplyPosition()
    {
        transform.position = targetPosition - (transform.forward * currentDistance);
    }

    private void ApplyZoom(
        float dt
        )
    {
        float dir;

        if (Input.GetKey(ZOOM_IN_KEY))
        {
            dir = -zoomKeyCoeff;
        }
        else
        {
            if (Input.GetKey(ZOOM_OUT_KEY))
            {
                dir = zoomKeyCoeff;
            }
            else
            {
                dir = -Input.GetAxis(MOUSE_WHEEL);
            }
        }

        ApplyZoom(dt, dir);
    }

    private void ApplyZoom(float dt, float dir)
    {
        targetDistance += dir * dt * zoomSpeed * Mathf.Abs(targetDistance);
        targetDistance = Mathf.Clamp(targetDistance, minDistance, maxDistance);
        currentDistance = Mathf.MoveTowards(currentDistance, targetDistance, dt * interpolationSpeed);
    }

    private void ApplyOrbitRotation(float dt)
    {
        float axisMultiplier = degSpeed * inputOrbitCoeff;
        xDeg += Input.GetAxis(MOUSE_X_AXIS) * axisMultiplier;
        yDeg -= Input.GetAxis(MOUSE_Y_AXIS) * axisMultiplier;
        yDeg = ClampAngle(yDeg, -yMax, yMax);

        Quaternion targetCamRotation = Quaternion.Euler(yDeg, xDeg, 0);
        Quaternion currentCamRotation = transform.rotation;

        transform.rotation = Quaternion.RotateTowards(currentCamRotation, targetCamRotation, dt * interpolationSpeed);
    }

    private static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -CIRCLE_ANGLE)
            angle += CIRCLE_ANGLE;
        if (angle > CIRCLE_ANGLE)
            angle -= CIRCLE_ANGLE;
        return Mathf.Clamp(angle, min, max);
    }

}
