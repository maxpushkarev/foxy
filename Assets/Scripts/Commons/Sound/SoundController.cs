﻿using UnityEngine;

public class SoundController : MonoBehaviour
{
	[SerializeField]
	private AudioSource source;
	[SerializeField]
	private float fadeInTime = 2;
	[SerializeField]
	private float fadeOutTime = 2;

	private float fadeInSpeed;
	private float fadeOutSpeed;
	private float speed;

	private void Awake()
	{
		enabled = false;
		source.volume = 0;
		fadeInSpeed = 1/fadeInTime;
		fadeOutSpeed = -1/fadeOutTime;
	}

	private void Update()
	{
		float currentVol = source.volume;
		currentVol += speed * Time.deltaTime;
		source.volume = Mathf.Clamp01(currentVol);

		if (currentVol >= 1)
		{
			enabled = false;
			return;
		}

		if (currentVol <= 0)
		{
			source.Stop();
			enabled = false;
		}
	}

	public void FadeIn()
	{
		if (!source.isPlaying)
		{
			source.Play();
		}
		speed = fadeInSpeed;
		enabled = true;
	}

	public void FadeOut()
	{
		speed = fadeOutSpeed;
		enabled = true;	
	}
}
