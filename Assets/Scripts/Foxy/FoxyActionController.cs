﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class FoxyActionController : MonoBehaviour {

    [SerializeField]
    private Animator animator;
    [SerializeField]
    private KeyCode runningKey;
    [SerializeField]
    private string runningName = "isRunning";

    private int runningID;

    private bool isRunning;
    private bool IsRunning
    {
        get
        {
            return isRunning;
        }
        set
        {
            if(value!=isRunning)
            {
                isRunning = value;
                animator.SetBool(runningID, isRunning);
            }
        }
    }

    private void OnEnable()
    {
        runningID = Animator.StringToHash(runningName);
        IsRunning = false;
    }

	private void Update()
	{
        IsRunning = Input.GetKey(runningKey);
    }
}