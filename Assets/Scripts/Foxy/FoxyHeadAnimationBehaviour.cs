﻿using UnityEngine;

public class FoxyHeadAnimationBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private string[] headIdleAnimationNames = new string[] 
    {
        "head1",
        "head2",
        "head3"
    };

    private int headIdleAnimationsCount;
    private int[] headIdleAnimationIDs;

    private void Awake()
    {
        headIdleAnimationsCount = headIdleAnimationNames.Length;
        headIdleAnimationIDs = new int[headIdleAnimationsCount];

        for (int i = 0; i < headIdleAnimationsCount; i++)
        {
            headIdleAnimationIDs[i] = Animator.StringToHash(headIdleAnimationNames[i]);
        }
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        int rnd = Random.Range(0, headIdleAnimationsCount);

        for(int i = 0; i<headIdleAnimationsCount; i++)
        {
            int trigger = headIdleAnimationIDs[i];
            if(i == rnd)
            {
                animator.SetTrigger(trigger);
            }
            else
            {
                animator.ResetTrigger(trigger);
            }
        }
    }
}
