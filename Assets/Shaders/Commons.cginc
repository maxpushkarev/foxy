#include "UnityCG.cginc"
#define UNITY5

uniform half4 _LightColor0;

const half PI = 3.141592;

void calculateLighting(half3 posWorld, out half3 lightDir, out half atten)
{

	#ifdef FORWARD_ADD
		if (_WorldSpaceLightPos0.w == 0.0) //check if directional light
        {
        	lightDir = normalize(_WorldSpaceLightPos0.xyz);
            atten = 1.0;
        } 
        else // point or spot light
        {
           half3 vectorToLightSource = _WorldSpaceLightPos0.xyz - posWorld.xyz;
          lightDir = normalize(vectorToLightSource);
            
            half distanceToLight = length(vectorToLightSource);     
            atten = 1.0 / distanceToLight;
         }
	#else
		lightDir = normalize(_WorldSpaceLightPos0.xyz);
		atten = 1.0;
	#endif
	
	#ifdef UNITY5
		atten *= 0.5f;
	#endif
	
}

half4 calculateAmbientPart(half4 mainColor)
{
	half4 ambientPart;
	ambientPart = UNITY_LIGHTMODEL_AMBIENT * mainColor;

	#ifdef UNITY5
	
	#else
		ambientPart *= 2.0;	
	#endif
	
	return ambientPart;
}


half3 calculateViewDir(half3 posWorld)
{
	return normalize(_WorldSpaceCameraPos - posWorld.xyz);
}

half3 decodeNormal(half4 encodedBump, half3 normalWorld, half3 tangentWorld, half3 binormalWorld)
{
	half3 localCoordsBump = half3(0.0, 0.0, 0.0);
    
   // #if defined(SHADER_API_GLES) && defined(SHADER_API_MOBILE)
     	//localCoordsBump = half3(encodedBump.x * 2.0 - 1.0, encodedBump.y * 2.0 - 1.0, encodedBump.z * 2.0 - 1.0);
 	//#else
		localCoordsBump = half3(encodedBump.w * 2.0 - 1.0, encodedBump.y * 2.0 - 1.0, 0.0);
    	localCoordsBump.z = sqrt(1.0 - dot(localCoordsBump, localCoordsBump));
	//#endif
    
	half3x3 local2WorldTranspose = half3x3(tangentWorld, binormalWorld, normalWorld);
	half3 normalDirection = normalize(mul(localCoordsBump, local2WorldTranspose));
	
	return normalDirection;
}


half3 setNormalIntensity(half3 normalDirection, half normalIntensity)
{
	return half3(normalDirection.x * normalIntensity, normalDirection.y * normalIntensity, normalDirection.z);
}

half4 calculatePhongPart(
				half3 nrm, 
				half3 lightDir, 
				half3 viewDir, 
				half atten, 
				half4 specColor, 
				half specPower
				)
{
	half NdotL = dot(nrm, lightDir);
	half3 refl = normalize(2.0 * nrm * max(0.0, NdotL) - lightDir);
		
	half spec = pow(max(0.0, dot(refl, viewDir)), specPower);
   	half4 modifiedSpecular = specColor * spec;
	half4 actualSpecularPart = _LightColor0 * modifiedSpecular * atten;
		
	#ifdef UNITY5
		actualSpecularPart *= 0.5;	
	#endif
	
	return actualSpecularPart;
}

half4 calculateBlinnPhongPart(half3 nrm, half3 lightDir, half3 viewDir, half4 specColor, half specPower, half atten)
{
	half3 h = normalize((lightDir+viewDir));
	half spec = pow(max(0.0, dot(nrm, h)), specPower);
	half4 modifiedSpecular = specColor * spec;
	half4 actualBlinnPhongPart = (_LightColor0 * modifiedSpecular * atten);
	
	#ifdef UNITY5
	
	#else
		actualBlinnPhongPart *= 2.0;
	#endif
	
	return actualBlinnPhongPart;
}

half4 calculateRimLighting(half3 nrm, half3 viewDir, half4 rimColor, half rimPower)
{
	half rim = 1.0 - saturate(dot(viewDir, nrm));
	half4 rimLightingPart = rimColor * pow(rim, rimPower);
	
	return rimLightingPart;
}

half4 mix(half4 x, half4 y, half a)
{
	half4 res =  x * (1.0 - a) + y*a;
	return res;
}