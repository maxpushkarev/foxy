// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

#include "./Commons.cginc"

uniform sampler2D _MainTex, _InfoMap;
  
uniform sampler2D _BumpMap;
uniform half _BumpIntensity;   
                  
uniform half4 _SpecularColor;
uniform half _SpecularPower;

uniform half4 _RimColor;
uniform half _RimPower;

uniform half4 _VelvetShinyTint, _VelvetSheenTint, _VelvetAmbientTint, _VelvetDiffuseTint;
uniform half _VelvetRoughness, _VelvetBackscatter, _VelvetEdginess;

uniform half _FurLength, _FurCutoff, _FurEdgeFade;

struct vertexInput {
     half4 vertex : POSITION;
     half3 normal : NORMAL;
     half4 tangent : TANGENT;
     half2 uv : TEXCOORD0;
};

 struct vertexOutput {
     half4 pos : SV_POSITION;
     half2 uv : TEXCOORD0;
     half4 posWorld : TEXCOORD1;
     half3 normalWorld : TEXCOORD2;
     half3 tangentWorld : TEXCOORD3;
     half3 binormalWorld : TEXCOORD4;
     half furAlpha : COLOR;  
};


half4 diffuseVector(half3 nrm, half3 vec)
{
   	half NdotVec = dot(nrm, vec);
   	return half4(NdotVec, NdotVec, NdotVec, NdotVec);
}

vertexOutput vert(vertexInput input) 
{
     vertexOutput output;
 
     half4x4 modelMatrix = unity_ObjectToWorld;
     half4x4 modelMatrixInverse = unity_WorldToObject; 
 
 	 float4 inputVertex = input.vertex;
	 inputVertex.xyz += input.normal * _FurLength * FUR_LAYER;
 
     output.posWorld = mul(modelMatrix, input.vertex);
     output.tangentWorld = normalize(mul(modelMatrix, float4(input.tangent.xyz, 0.0)).xyz);
     output.normalWorld = normalize(mul(half4(input.normal, 0.0), modelMatrixInverse).xyz);
     output.binormalWorld = normalize(cross(output.normalWorld, output.tangentWorld) * input.tangent.w); 
     
     output.pos = mul(UNITY_MATRIX_MVP, inputVertex);
     output.uv = input.uv;
     
     
     float alpha = 1.0 - pow(FUR_LAYER, 2.0);	
	 half3 viewDir = normalize(ObjSpaceViewDir(input.vertex));	
	 alpha += dot(viewDir, input.normal) - _FurEdgeFade;
     
     output.furAlpha = alpha;
     
     return output;
}

half4 frag(vertexOutput input) : COLOR
{
	half4 mainColor = tex2D(_MainTex, input.uv);
	half4 info = tex2D (_InfoMap, input.uv);
	
	half3 normalDirection = normalize(input.normalWorld);
	
	#ifdef USE_BUMP
		half4 encodedBump = tex2D(_BumpMap, input.uv);
		normalDirection = decodeNormal(encodedBump, input.normalWorld, input.tangentWorld, input.binormalWorld);
		normalDirection = setNormalIntensity(normalDirection, _BumpIntensity);
	#endif
	
	half3 viewDirection = calculateViewDir(input.posWorld.xyz);
	
	half3 lightDirection;
	half attenuation;
	calculateLighting(input.posWorld, lightDirection, attenuation);
	
	half4 ambientPart = calculateAmbientPart(mainColor);
	
	half4 diffusePart = _LightColor0 * mainColor;
	half NdotL = max(0.0, dot(normalDirection, viewDirection)); 
	diffusePart *= max(0.0, NdotL) * attenuation;	
	
	
	half4 basePart = ambientPart + diffusePart;
		
	#ifdef USE_VELVET

	half3 normalizedNRM = normalize(normalDirection);
	//half cosineLV = dot(viewDirection, viewDirection);
	half cosineLV = 1.0f;
	half cosineNV = dot(normalizedNRM, viewDirection);
	half sine = sqrt(1.0 - pow(cosineNV, 2.0));

	half4 velvetPart = half4(0.0, 0.0, 0.0, 0.0);
	half4 local_shiny = _VelvetShinyTint;
	half4 velvetAdditivePart = calculateAmbientPart(velvetPart);

	half4 velvetAmbientPart = _VelvetAmbientTint * mainColor * attenuation;
	half velvetDiff = diffuseVector(normalDirection, viewDirection) * 0.5 + 0.5;
	half4 velvetDiffusePart = _VelvetDiffuseTint * velvetDiff * mainColor * attenuation;

	half4 velvetSpecularNV = pow(sine, _VelvetEdginess) * NdotL * _LightColor0 * _VelvetSheenTint * attenuation;
	#ifdef UNITY5
		velvetSpecularNV *= 0.5;
	#endif

	local_shiny += velvetSpecularNV;

	half4 velvetSpecularLV = pow(abs(cosineLV), 1.0 / _VelvetRoughness) * _VelvetBackscatter * _LightColor0 * _VelvetSheenTint;
	#ifdef UNITY5
		velvetSpecularLV *= 0.5;
	#endif

	local_shiny += velvetSpecularLV;


	half4 emptyShiness = half4(0.0, 0.0, 0.0, 0.0);
	local_shiny = lerp(local_shiny, emptyShiness, info.g);

	velvetPart = velvetAmbientPart + velvetDiffusePart + local_shiny;
	basePart = lerp(velvetPart + velvetAdditivePart, basePart, info.r);
		
	#endif											
											
	half4 resColor = basePart;
	
	#ifdef USE_SPECULAR
	
		half4 specularPart = half4(0.0, 0.0, 0.0, 0.0);
		half4 actualSpecularPart = calculatePhongPart(
												normalDirection, 
												viewDirection, 
												lightDirection, 
												attenuation, 
												_SpecularColor, 
												_SpecularPower
									);
									
		specularPart = lerp(specularPart, actualSpecularPart, info.r);
		
		resColor += specularPart;
	#endif
	
	#ifdef USE_RIM
	
		half4 emissionPart = calculateRimLighting(normalDirection, viewDirection, _RimColor, _RimPower);
		
		resColor += emissionPart;
	#endif
	
	#ifdef FIXED_ALPHA
		resColor.a = FIXED_ALPHA;
	#else
		resColor.a = mainColor.a * input.furAlpha;
	#endif
	
	return resColor;
}

