﻿Shader "Foxy/FoxyFull" {

   Properties {
   
   		_MainTex ("Main Foxy Texture", 2D) = "white" {}
		_InfoMap ("Info Map", 2D) = "white" {}
		
		_BumpMap ("Bump Map", 2D) = "bump" {}
		_BumpIntensity ("Bump Intensity", Range(0.0, 10.0)) = 1.0
		
		_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
      	_RimPower ("Rim Power", Range(0.1,16.0)) = 3.0
      	
      	_SpecularColor ("Specular Color", color) = (1.0,1.0,1.0,1.0)
		_SpecularPower ("Specular Power", Range(0.0, 0.5)) = 0.25
		
		_VelvetShinyTint ("Velvet Shiny Tint", Color) = (0.1267, 0.1267, 0.1267, 1.0)
		_VelvetSheenTint ("Velvet Sheen Tint", Color) = (0.8137, 0.8137, 0.8137, 1.0)
		_VelvetAmbientTint ("Velvet Ambient Tint", Color) = (0.23, 0.23, 0.23, 1.0)
		_VelvetDiffuseTint ("Velvet Diffuse Tint", Color) = (0.43, 0.43, 0.43, 1.0)
		
		_VelvetRoughness ("Velvet Roughness", Range(0.0, 3.0)) = 0.1
		_VelvetBackscatter ("Velvet Backscatter", Range(0.0, 3.0)) = 0.1
		_VelvetEdginess ("Velvet Edginess", Range(0.0, 30.0)) = 20.5
		
		_FurLength ("Fur Length", Range (0.01, 0.05)) = 0.05
		_FurCutoff ("Fur Alpha Cutoff", Range (0.0, 0.1)) = 0.0001
		_FurEdgeFade ("Fur Edge Fade", Range(0.0, 0.8)) = 0.4
		
   }
   
   Category {
   	
   		ZWrite Off
   		Tags 
   		{
   			"Queue" = "Transparent"
   			"LightMode" = "ForwardBase"
   		}
   		Blend SrcAlpha OneMinusSrcAlpha
		Alphatest Greater [_FurCutoff]
   		
   		SubShader {
   
      			Pass { 
 						
 						ZWrite On
						Blend Off
 						
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_SPECULAR
 							#define USE_RIM
							#define USE_VELVET				

 							#define FIXED_ALPHA 1.0
 							#define FUR_LAYER 0.0
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}
     			

     			Pass { 
 					
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_RIM
							#define USE_VELVET
 							
 							#define FUR_LAYER 0.1
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}
     			
				Pass { 
 					
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_RIM
							#define USE_VELVET
 							
 							#define FUR_LAYER 0.2
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}

				Pass { 
 					
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_RIM
							#define USE_VELVET
 							
 							#define FUR_LAYER 0.3
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}

     			Pass { 
 					
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_RIM
							#define USE_VELVET
 							
 							#define FUR_LAYER 0.4
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}
     			
			Pass { 
 					
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_RIM
							#define USE_VELVET	
 							
 							#define FUR_LAYER 0.5
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}

     			Pass { 
 					
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_RIM
							#define USE_VELVET
 							
 							#define FUR_LAYER 0.6
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}
     			
				Pass { 
 					
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_RIM
							#define USE_VELVET
 							
 							#define FUR_LAYER 0.7
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}

     			Pass { 
 					
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_RIM
							#define USE_VELVET
 							
 							#define FUR_LAYER 0.8
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}
     			
			Pass { 
 					
         				CGPROGRAM

 							#define USE_BUMP
 							#define USE_RIM
							#define USE_VELVET

 							#define FUR_LAYER 0.9
 							
 		 					#include "FoxyEffects.cginc"
 		 					#pragma target 3.0
 		 					#pragma fragmentoption ARB_precision_hint_nicest
							#pragma glsl_no_auto_normalization
         					#pragma vertex vert  
         					#pragma fragment frag 

         				ENDCG
     			}
      			
			Pass{

				CGPROGRAM

				#define USE_BUMP
				#define USE_RIM
				#define USE_VELVET

				#define FUR_LAYER 1.0

				#include "FoxyEffects.cginc"
				#pragma target 3.0
				#pragma fragmentoption ARB_precision_hint_nicest
				#pragma glsl_no_auto_normalization
				#pragma vertex vert  
				#pragma fragment frag 

				ENDCG
			}

   		}
   		
   		Fallback "Diffuse"
   }
   
}