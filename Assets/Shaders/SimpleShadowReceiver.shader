﻿Shader "SimpleShadowReceiver" {

Properties {
		_Color ("Main Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_ShadowForce ("Shadow Force", Range(0.0, 1.0)) = 1.0
	}

	SubShader {
	
		Tags {
			"Queue" = "Geometry" 
			"RenderType"="Opaque"
			"LightMode" = "ForwardBase" 
		}
	
		Pass
		{			
			CGPROGRAM
			
				#include "Commons.cginc"
				#include "AutoLight.cginc"
				
 		 		#pragma target 3.0   		
         		#pragma vertex vert  
         		#pragma fragment frag		
				
				#pragma multi_compile_fwdbase 

				half4 _Color;
				half _ShadowForce;

				struct vertexInput {
					half4 vertex : POSITION;
				};

				struct vertexOutput {
					half4 pos : SV_POSITION;
					SHADOW_COORDS(0)
				};


				vertexOutput vert(vertexInput v)
				{
					vertexOutput output;
					output.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					TRANSFER_SHADOW(output);
					return output;
				}

				half4 frag(vertexOutput input) : COLOR
				{
					half shadowAttenuation = SHADOW_ATTENUATION(input);
					shadowAttenuation += _ShadowForce;
					shadowAttenuation = saturate(shadowAttenuation);
					half4 fragColor = _Color;
					fragColor *= shadowAttenuation;
					return fragColor;
				}
				
			ENDCG
		}

	} 

	Fallback "Diffuse"
}